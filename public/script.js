const image = document.querySelector("#imgdiv")
const arrow = document.querySelector("#arrow")
const github = document.querySelector('.github-login')

image.addEventListener("click", () =>{
  window.location.href = "https://github.com/codeplanett"
})

arrow.addEventListener("click", () =>{
  window.location.href = "https://github.com/codeplanett"
})

github.addEventListener("click", () =>{
  window.location.href = "https://codeplanet.glitch.me/login/github"
})
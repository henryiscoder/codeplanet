# CodePlanet

The platform which can teach you programming.

#  Team

[Here](https://github.com/codeplanett), You can browse our organization to reach people who help and support us. If you want to help us too, keep reading.

# Features 

- [x] Landing Page
- [x] Beautiful Design
- [x] Login
- [x] Register
- [x] User Dashboard
- [x] Post Dashboard
- [x] Post Articles
- [x] Reporting System
- [x] Projects
- [x] Blog

# CONTIRIBUTING

We have some terms and requirements that we present to you to help with this project.
As you know, since the project is managed by a verified organization, your [2FA](https://github.com/settings/security) must be turned on in your account.
As a result of your help, your chance to join our verified field organization will increase.

Since the project is a project that receives [MARKDOWN](https://www.markdownguide.org) support and usually uses this type of font in its wiki, you should be able to perform simple markdown operations.

![opera_7rWPINZVA0](https://user-images.githubusercontent.com/31706007/123513180-5d139380-d694-11eb-8b30-86cda7048004.png)
To help us, you must both start and fork the project.

After forking, you need to make the changes from the codeplanet repository that comes to your profile, and then when you come to the main repository, you need to go to pull requests to provide what we need to merge, and then you can pull requests from the examples that appear.
